package net.diamondmine.debugger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.logging.Level;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * LogFile class.
 * 
 * @author Jon la Cour
 * @since 1.2.1
 */
public class LogFile implements Serializable {
    private static final long serialVersionUID = 938904900815918284L;
    public String name = String.valueOf(System.currentTimeMillis());
    public StackTraceElement[] stack;
    public String exceptionMessage;

    /**
     * Constructor.
     * 
     * @param stackTrace
     *            Exception stacktrace.
     * @param message
     *            Exception message.
     */
    public LogFile(final StackTraceElement[] stackTrace, final String message) {
        stack = stackTrace;
        exceptionMessage = message;
    }

    /**
     * Saves an error file.
     * 
     * @param plugin
     *            Plugin class
     * @param action
     *            Action that triggered logging.
     */
    public final void save(final JavaPlugin plugin, final String action) {
        String separator = System.getProperty("file.separator");
        String newline = System.getProperty("line.separator");
        
        File dataFolder = new File("plugins" + separator + plugin.getName() + separator + "log");
        if (!dataFolder.exists()) {
            dataFolder.mkdirs();
        }
        
        String file = "plugins" + separator + plugin.getName() + separator + "log" + separator + name + ".txt";
        String error = action + newline;
        error += exceptionMessage + newline;
        for (StackTraceElement line : stack) {
            error += line.toString() + newline;
        }

        PrintStream out = null;
        try {
            out = new PrintStream(new FileOutputStream(file));
            out.print(error);
        } catch (Exception e) {
            plugin.getLogger().log(Level.WARNING, "[" + plugin.getName() + "] Error saving log file " + name + ".txt.");
        } finally {
            out.close();
        }
    }
}
