package net.diamondmine.reloader;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import net.diamondmine.debugger.LogFile;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of PluginReloader.
 * 
 * @author Jon la Cour
 * @version 1.3.1
 */
public class PluginReloader extends JavaPlugin {
    public static final Logger logger = Bukkit.getLogger();

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        log("Version " + getDescription().getVersion() + " is disabled!", "info");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        log("Version " + getDescription().getVersion() + " enabled", "info");
    }

    @Override
    public final boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, final String[] args) {
        if (cmd.getName().equalsIgnoreCase("plugin")) {
            if (args.length < 1) {
                return false;
            }
            String action = args[0];

            if (!(action.equalsIgnoreCase("load") || action.equalsIgnoreCase("unload") || action.equalsIgnoreCase("reload"))) {
                send("Invalid action specified.", sender);
                return false;
            }

            if (!sender.hasPermission("pluginreloader." + action)) {
                send("You do not have the permission to do this.", sender);
                return true;
            }

            if (args.length == 1) {
                send("You must specify at least one plugin.", sender);
                return true;
            }

            for (int i = 1; i < args.length; ++i) {
                String plName = args[i];

                if (plName.startsWith("\"") && args.length > 2) {
                    if (args[i + 1].endsWith("\"")) {
                        plName += " " + args[i + 1];
                        plName = plName.replace("\"", "");
                        i++;
                    }
                }

                try {
                    if (action.equalsIgnoreCase("unload")) {
                        unloadPlugin(plName, true, sender);
                    } else if (action.equalsIgnoreCase("load")) {
                        loadPlugin(plName, true, sender);
                    } else if (action.equalsIgnoreCase("reload")) {
                        reloadPlugin(plName, true, sender);
                    }
                } catch (Exception e) {
                    send("Error with " + ChatColor.RED + plName + ChatColor.GOLD + ": Report error with dump file.", sender);
                    LogFile log = new LogFile(e.getStackTrace(), e.getMessage());
                    log.save(this, action + " " + plName);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Unloads a plugin by name.
     * 
     * @param pluginName
     *            The name of the plugin to unload.
     * @param errorHandling
     *            True if we should send errors to command sender.
     * @param sender
     *            The command sender for error handling.
     * @return boolean depending on success
     * @throws Exception
     *             Thrown when a plugin cannot be unloaded.
     * @since 1.0.0
     */
    @SuppressWarnings("unchecked")
    private boolean unloadPlugin(final String pluginName, final boolean errorHandling, final CommandSender sender) throws Exception {
        PluginManager manager = getServer().getPluginManager();
        SimplePluginManager spmanager = (SimplePluginManager) manager;

        if (spmanager != null) {
            Field pluginsField = spmanager.getClass().getDeclaredField("plugins");
            pluginsField.setAccessible(true);
            List<Plugin> plugins = (List<Plugin>) pluginsField.get(spmanager);

            Field lookupNamesField = spmanager.getClass().getDeclaredField("lookupNames");
            lookupNamesField.setAccessible(true);
            Map<String, Plugin> lookupNames = (Map<String, Plugin>) lookupNamesField.get(spmanager);

            Field commandMapField = spmanager.getClass().getDeclaredField("commandMap");
            commandMapField.setAccessible(true);
            SimpleCommandMap commandMap = (SimpleCommandMap) commandMapField.get(spmanager);

            Field knownCommandsField = null;
            Map<String, Command> knownCommands = null;

            if (commandMap != null) {
                knownCommandsField = commandMap.getClass().getDeclaredField("knownCommands");
                knownCommandsField.setAccessible(true);
                knownCommands = (Map<String, Command>) knownCommandsField.get(commandMap);
            }

            for (Plugin plugin : manager.getPlugins()) {
                if (plugin.getDescription().getName().equalsIgnoreCase(pluginName)) {
                    manager.disablePlugin(plugin);

                    if (plugins != null && plugins.contains(plugin)) {
                        plugins.remove(plugin);
                    }

                    if (lookupNames != null && lookupNames.containsKey(pluginName)) {
                        lookupNames.remove(pluginName);
                    }

                    if (commandMap != null) {
                        for (Iterator<Map.Entry<String, Command>> it = knownCommands.entrySet().iterator(); it.hasNext();) {
                            Map.Entry<String, Command> entry = it.next();

                            if (entry.getValue() instanceof PluginCommand) {
                                PluginCommand command = (PluginCommand) entry.getValue();

                                if (command.getPlugin() == plugin) {
                                    command.unregister(commandMap);
                                    it.remove();
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (errorHandling) {
                send(pluginName + " is already unloaded.", sender);
            }
            return true;
        }

        if (sender instanceof Player && !errorHandling) {
            log(sender.getName() + " has unloaded " + pluginName + ".", "info");
        }

        if (errorHandling) {
            send("Unloaded " + pluginName + " successfully!", sender);
        }

        return true;
    }

    /**
     * Loads a plugin by name.
     * 
     * @param pluginName
     *            The name of the plugin to load.
     * @param errorHandling
     *            True if we should send errors to command sender.
     * @param sender
     *            The command sender for error handling.
     * @return boolean depending on success
     * @since 1.0.0
     */
    private boolean loadPlugin(final String pluginName, final boolean errorHandling, final CommandSender sender) {
        try {
            PluginManager manager = getServer().getPluginManager();
            Plugin plugin = manager.loadPlugin(new File("plugins", pluginName + ".jar"));

            if (plugin == null) {
                if (errorHandling) {
                    send("Error loading " + pluginName + ", no plugin with that name was found.", sender);
                }
                return false;
            }

            plugin.onLoad();
            manager.enablePlugin(plugin);
        } catch (Exception e) {
            if (errorHandling) {
                send("Error loading " + pluginName + ", this plugin must be reloaded by restarting the server.", sender);
            }

            return false;
        }

        if (sender instanceof Player && !errorHandling) {
            log(sender.getName() + " has loaded " + pluginName + ".", "info");
        }

        if (errorHandling) {
            send("Loaded " + pluginName + " successfully!", sender);
        }

        return true;
    }

    /**
     * Reloads a plugin by name.
     * 
     * @param pluginName
     *            The name of the plugin to reload.
     * @param errorHandling
     *            True if we should send errors to command sender.
     * @param sender
     *            The command sender for error handling.
     * @return boolean depending on success
     * @throws Exception
     *             Thrown when a plugin cannot be unloaded.
     * @since 1.0.3
     */
    private boolean reloadPlugin(final String pluginName, final boolean errorHandling, final CommandSender sender) throws Exception {
        boolean unload = unloadPlugin(pluginName, false, sender);
        boolean load = loadPlugin(pluginName, false, sender);

        if (sender instanceof Player) {
            log(sender.getName() + " reloaded " + pluginName + ".", "info");
        }

        if (unload && load) {
            if (errorHandling) {
                send("Reloaded " + pluginName + " successfully!", sender);
            }
        } else {
            if (errorHandling) {
                send("Error reloading " + pluginName + ".", sender);
            }

            return false;
        }

        return true;
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.0
     */
    public static void log(final String s, final String type) {
        String message = "[PluginReloader] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends the command sender a message.
     * 
     * @param message
     *            Message to send to CommandSender.
     * @param sender
     *            CommandSender to send message to.
     * @since 1.3.1
     */
    private void send(final String message, final CommandSender sender) {
        if (sender == null) {
            log(message, "info");
        } else {
            sender.sendMessage(ChatColor.GOLD + message);
        }
    }
}
