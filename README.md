# PluginReloader

A Bukkit server plugin for Minecraft that allows for easy plugin management.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=RELOADER)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/RELOADER-RELOADER)**
-- **[Download](http://diamondmine.net/plugins/download/PluginReloader)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/pluginreloader/)**
